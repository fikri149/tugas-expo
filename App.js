import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { Button } from 'native-base';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import Homes from './Menu/Home/Homes'
import Profile from './Menu/Profile/Profile'

class Masakan extends Component {
  render() {
    return (
      <View>
        <StatusBar style="auto" />
        <View>
          <Text>Masakan</Text>
        </View>
      </View>
    );
  }
  
}

class Kota extends Component {
  render() {
    return (
      <View>
        <StatusBar style="auto" />
        <View>
          <Text>Kota</Text>
        </View>
      </View>
    );
  }
  
}



const HomeStack = createStackNavigator();
const MasakanStack = createStackNavigator();
const KotaStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Homes} />
  </HomeStack.Navigator>
)

const MasakanStackScreen = () => (
  <MasakanStack.Navigator>
    <MasakanStack.Screen name="Makanan" component={Masakan} />
  </MasakanStack.Navigator>
)

const KotaStackScreen = () => (
  <KotaStack.Navigator>
    <KotaStack.Screen name="Kota" component={Kota} />
  </KotaStack.Navigator>
)

const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
)

const Router = createBottomTabNavigator();

function RouterStack() {
  return (
    <NavigationContainer>
      <Router.Navigator 
      initialRouteName={'Home'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? 'home'
              : 'home';
          } else if (route.name === 'Masakan') {
            iconName = focused ? 'book' : 'book';
          } else if (route.name === 'Kota') {
            iconName = focused ? 'building' : 'building';
          } else if (route.name === 'Profile') {
            iconName = focused ? 'user' : 'user';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}
      >
        <Router.Screen name="Home" component={Homes} />
        <Router.Screen name="Masakan" component={MasakanStackScreen} />
        <Router.Screen name="Kota" component={KotaStackScreen} />
        <Router.Screen name="Profile" component={ProfileStackScreen} />
      </Router.Navigator>
      
    </NavigationContainer>
  );
}

export default RouterStack;
