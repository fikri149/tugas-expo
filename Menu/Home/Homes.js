import React, {Component} from 'react';
import { Text, View, Image } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { StatusBar } from 'expo-status-bar';
import { Content, Card, CardItem, Left, Icon, Right } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from "axios";

export default class Homes extends Component {

    constructor(){
        super();
        this.state = {
            images:[
                "https://images.pexels.com/photos/1161468/pexels-photo-1161468.jpeg?auto=compress&cs=tinysrgb&dpr=18w=500",
                "https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg?auto=compress&cs=tinysrgb&dpr=18w=500",
                "https://images.pexels.com/photos/2313686/pexels-photo-2313686.jpeg?auto=compress&cs=tinysrgb&dpr=18w=500"

            ],
            dataCategory:[],
            dataRestaurant:[],
        }
    }

    getDataCategory=() =>{
        axios.get('https://developers.zomato.com/api/v2.1/categories',
        {
            headers:{"user-key":"90965527095adf33591d245e7bb747e4"}
        })
        .then((res)=>{
            this.setState({
                dataCategory: res.data.categories
            })
        })
    }

    getDataRestaurant=() =>{
        axios.get('https://developers.zomato.com/api/v2.1/search?start=6&count=12&sort=rating',
        {
            headers:{"user-key":"90965527095adf33591d245e7bb747e4"}
        })
        .then((res)=>{
            this.setState({
                dataRestaurant: res.data.restaurants
            })
        })
    }

    componentDidMount() {
        this.getDataCategory();
        this.getDataRestaurant();
    }
    render() {
        return (
            <View style={{flex:1}}>
                <StatusBar backgroundColor="#f85252"/>
                <Content>
                <View style={{marginTop:23, height:185}}>
                    <ImageSlider
                    autoPlayWithInterval={3000}
                    images= {this.state.images} />
                </View>

                <Text style={{ marginTop:20, marginLeft:10, fontSize:18}}>Pilihan Kategori</Text>
                <Content horizontal style={{ marginTop:20, marginLeft:10}}>
                    {
                        this.state.dataCategory.map((data, key)=>{
                            return (
                                <TouchableOpacity key={key} style={{margin:5, backgroundColor: "#f85252", borderRadius:25, height:40, justifyContent:"center", alignItems:"center"}}>
                                    <Text style={{ color:"white"}}>   {data.categories.name}   </Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                    
                    
                </Content>

                <Text style={{ marginTop:20, marginLeft:10, fontSize:18}}>Restaurant Terbaik</Text>
                
                {
                    this.state.dataRestaurant.map((data, key)=>{

                        var image = ""
                        if(data.restaurant.thumb === "") {
                            image = "https://topekacivictheatre.com/wp-content/uploads/2019/01/no-image.jpg"
                        } else {
                            image = data.restaurant.thumb
                        }
                        return (
                            <Card key={key}>
                                <CardItem>
                                    <Text>{data.restaurant.name}</Text>
                                </CardItem>
                                <CardItem cardBody>
                                    <Image
                                    style={{ height:200, width:null, flex: 1}}
                                    source={{ uri: image }}/>
                                </CardItem>
                                <CardItem>
                                    <Left>
                                        <Icon name="star" />
                                    <Text>{ data.restaurant.user_rating.aggregate_rating }</Text>
                                    </Left>
                                    <Right>
                                        <Text>{ data.restaurant.user_rating.rating_text }</Text>
                                    </Right>
                                </CardItem>
                            </Card>
                        )
                    })
                }

                            
                
                
                </Content>
            </View>
        )
    }
}