import React, {Component} from 'react';
import { Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Profile extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:2, justifyContent:"center", alignItems:"center"}}>
                    <Image style={{ marginTop:70, height:300, width:300}} source={require('../assets/logo.png')} />
                    <Text style={{ fontSize:50 }}>RestoQu</Text>
                </View>
                <View style={{flex:1, flexDirection:"row", marginTop:50, justifyContent:"center", alignItems:"center"}}>
                    <View style={{margin:8, justifyContent:"center", alignItems:"center"}}>
                        <Icon name= "instagram" size={60} color="#f85252" />
                        <Text>@fickreey_149</Text>
                    </View>
                    <View style={{margin:8, justifyContent:"center", alignItems:"center"}}>
                        <Icon name= "telegram" size={60} color="#f85252" />
                        <Text>FikriHidayat</Text>
                    </View>
                    <View style={{margin:8, justifyContent:"center", alignItems:"center"}}>
                        <Icon name= "gitlab" size={60} color="#f85252" />
                        <Text>@fikri149</Text>
                    </View>
                    <View style={{margin:8, justifyContent:"center", alignItems:"center"}}>
                        <Icon name= "github" size={60} color="#f85252" />
                        <Text>@fikri149</Text>
                    </View>
                </View>
            </View>
        )
    }
}