import React, { Component } from 'react';
import { 
    Platform,
    Text,
    View,
    StyleSheet,
    Image,
    FlatList,
    TouchableOpacity,
    TextInput,
    Button
 } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Image source={require('./images/logo.png')} style={styles.logo} />
          <Text style={styles.title}>Login</Text>
          <View style={styles.inputan}>
            <View>
              <Text>Username / Email</Text>
              <TouchableOpacity>
                <TextInput style={{ height: 48, width:294, borderColor: '#003366', borderWidth: 1 }} />
              </TouchableOpacity>
            </View>
            <View>
              <Text style={{ paddingTop: 16}}>Password</Text>
              <TouchableOpacity>
                <TextInput style={{ height: 48, width:294, borderColor: '#003366', borderWidth: 1 }} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={{ paddingTop:32}}>
              <View style={styles.buttonMasuk}>
                <Text style={styles.textMasuk}>Masuk</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ paddingTop:10}}>
              <View style={styles.buttonDaftar}>
                <Text style={styles.textDaftar}>Daftar ?</Text>
              </View>
            </TouchableOpacity>
            
          </View>

            {/* <FlatList
                data={data.items}
                renderItem={(video)=><VideoItem video={video.item} />}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
            /> */}
        </View>
        {/* <View style={styles.tabBar}>
            <TouchableOpacity style={styles.tabItem} size={25}>
                <Icon name="home" size={25}/>
                <Text style={styles.tabTitle}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem} size={25}>
                <Icon name="whatshot" size={25}/>
                <Text style={styles.tabTitle}>Trending</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem} size={25}>
                <Icon name="subscriptions" size={25}/>
                <Text style={styles.tabTitle}>Subscriptions</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem} size={25}>
                <Icon name="folder" size={25}/>
                <Text style={styles.tabTitle}>Library</Text>
            </TouchableOpacity>

        </View> */}
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25,
  },
  body:{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center'
  },
  logo:{
      width:375,
      height:102,
      paddingTop: 63
  },
  title:{
      fontSize: 24,
      color: 'black',
      paddingTop: 70
  },
  inputan:{
      fontSize: 16,
      paddingTop:40,
      color: '#003366',
      alignItems: 'center'
  },
  buttonMasuk:{
    backgroundColor: '#3EC6FF',
    paddingVertical:12,
    paddingHorizontal:25,
    width: 140,
    height: 40,
    borderRadius: 32,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textMasuk:{
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 24,
    color: 'white'
  },
  buttonDaftar:{
    backgroundColor: '#003366',
    paddingVertical:12,
    paddingHorizontal:25,
    width: 140,
    height: 40,
    borderRadius: 32,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textDaftar:{
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 24,
    color: 'white'
  }
})